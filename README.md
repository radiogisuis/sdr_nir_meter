# SDR_RNI_Meter


## Prerequisites ##


### Host ###

* A Linux-based OS.
* GNU Radio.
* git.
* gr-RadioGIS blocks.

### Server (USRP) ###

* A Linux-based OS.
* GNU Radio.
* UHD 3.8.5.
* gr-RadioGIS blocks.
* git (optional).

Python 2.x is also required, but it comes by default with any Linux distribution.

## Running it ##

* clone this repository:  

```
#!bash

git clone https://bitbucket.org/radiogisuis/sdr_nir_meter
```


* Open SDR_RNI_GUI.py and SDR_RNI_Server.py using any text editor and update the IPs.

* Either copy SDR_RNI_Server.py and remote_configurator.py to the USRP or repeat the process above inside the USRP.

* Run SDR_RNI_Server.py in the USRP:  

```
#!bash

python SDR_RNI_Server.py
```


* Run SDR_RNI_GUI.py in the host:  

```
#!bash

python SDR_RNI_GUI.py windows
```
 
where *windows* is the number of windows to use, if not indicated it will default to 4.

* Configure the parameters (everything but the window size is configurable from the GUI) and click "Iniciar". If you want to stop the program before it terminates, simply click "Parar".

## Known issues ##

* The quality of the results strongly depends on the network (bandwidth, congestion, etc.).
* Once you click the "Iniciar" button, you will need to restart the program to try new configurations (both in the USRP and in the host).
* USRPs sometimes behave weirdly (and hence, the results are not as good as we would like).


If you find any additional issues or, want to suggest an improvement or request the addition of new features, please write to jmunoz@radiogis.uis.edu.co or dacosta@radiogis.uis.edu.co.